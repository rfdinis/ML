clear; clc; close all;

data = load('data2.txt');
X = data(:, 1:2);
y = data(:, 3);
[X mu sigma] = featureNormalize(X);
X = [ones(length(y), 1) X];

a = [0.003 0.01 0.03 0.1 0.3 1];
num_iters = 200;

J = zeros(length(a), num_iters)';
for i = 1:length(a)
	alpha = a(i);
	theta = zeros(3, 1);
	[theta, J(:,i)] = gradientDescent(X, y, theta, alpha, num_iters);
end

figure
subplot(2,1,1);
plot(J);
legend(cellstr(num2str(a', 'alpha=%-d')))

subplot(2,1,2);
plot(a, J(end,:))