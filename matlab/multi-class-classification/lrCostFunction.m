function [J, grad] = lrCostFunction(theta, X, y, lambda)
%LRCOSTFUNCTION Compute cost and gradient for logistic regression with 
%regularization
%   J = LRCOSTFUNCTION(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 


% useful variables
m    = length(y); 			  % number of training examples
h    = sigmoid(X * theta);    % hypothesis
temp = theta; temp(1,1) = 0;  % ignore theta0 from regularization

% cost
cost = 1/m * (-y'*log(h) - (1-y)'*log(1-h));  % unregularized cost func
reg  = lambda / 2 / m * sum(temp.^2);         % regularization
J    = cost + reg;

% gradients
greg = lambda / m * temp;  			% grad regularization
grad = 1 / m * X' * (h - y) + greg;
grad = grad(:);

end
