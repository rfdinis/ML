%% Initialization
clear ; close all; clc

%% Setup
input_layer_size  = 400;  % 20x20 Input Images of Digits
num_labels = 10;          % 10 labels, from 1 to 10
                          % (note: "0" mapped to label 10)
load('data.mat');         % training data stored in arrays X, y

%% Visualize data
m = size(X, 1);
rand_indices = randperm(m); % Randomly select 100 data points to display
sel = X(rand_indices(1:100), :);
displayData(sel);

%% Training
fprintf('Training One-vs-All Logistic Regression...\n');
[all_theta] = oneVsAll(X, y, num_labels, 0.1);

%% Prediction
pred = predictOneVsAll(all_theta, X);
fprintf('\nTraining Set Accuracy: %f\n', mean(double(pred == y)) * 100);
