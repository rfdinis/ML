function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

m = size(X, 1);


%% Feed Forward
a1 = X;
a2 = sigmoid([ones(m, 1), a1] * Theta1');
a3 = sigmoid([ones(m, 1), a2] * Theta2');
h = a3;

% vectorized theta without the ones
th   = [Theta1(:, 2:end)(:) ; Theta2(:, 2:end)(:)];  
reg  = lambda/2/m * sum(th.^2);

ny = eye(num_labels)(y, :);  % need to change y
cost = -ny .* log(h) - (1-ny) .* log(1-h);  % no, replacing .* by * does not solve it
J = 1/m * sum(cost(:)) + reg;

d2 = 0; d1 = 0;

%% Back propagation algorithm
for i = 1:m
% forward
a1 = [1; X(i,:)'];
z2 = Theta1 * a1;
a2 = [1; sigmoid(z2)];
z3 = Theta2 * a2;
a3 = sigmoid(z3);

% error propagation
e3 = a3 - ny(i,:)';
e2 = (Theta2(:, 2:end)' * e3) .* sigmoidGradient(z2);

% accumulate deltas
d2 = d2 + (e3 * a2');
d1 = d1 + (e2 * a1');

end

%% Regularization
Theta1_grad = 1/m * d1;
Theta2_grad = 1/m * d2;

Theta1_grad(:, 2:end) += lambda/m * Theta1(:, 2:end);
Theta2_grad(:, 2:end) += lambda/m * Theta2(:, 2:end);

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];
end
