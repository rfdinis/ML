function [error_train, error_val] = ...
    learningCurveRandom(X, y, Xval, yval, lambda)

	
m = size(X,1)     % the number of training examples
r = size(Xval,1)  % the number of validation examples

for i = 1:m
  fprintf('****** it %d ******', i);
  % create two empty vectors for the Jtrain and Jcv values
  Jtrain = zeros(50, 1);
  Jcv = zeros(50, 1);
  for j = 1:50
	% 'i' random examples from the training set
	v = randperm(m, i);
	xtemp = X(v,:);
	ytemp = y(v);
    
    % 'i' random examples from the validation set
    v = randperm(r, i);
	xvtemp = Xval(v,:);
	yvtemp = yval(v);
	
	% compute theta
	[theta] = trainLinearReg(xtemp, ytemp, lambda);
	
    % compute Jtrain and Jcv and save the values
	[Jtrain(j), dummy] = ...
		linearRegCostFunction(xtemp, ytemp, theta, 0);
	[Jcv(j), dummy] = ...
		linearRegCostFunction(xvtemp, yvtemp, theta, 0);
  end
  error_train(i) = mean(Jtrain);
  error_val(i) = mean(Jcv);
end
end
