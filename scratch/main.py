import numpy as np

class tensor(np.ndarray): pass

class layer:
    def __init__(self):
        raise NotImplementedError()
    def forward(self, input):
        raise NotImplementedError()
    def __call__(self, input):
        return self.forward(input)
    

class linear(layer):
    def __init__(self, inputs, outputs):
        rand_weights = np.random.random_sample((outputs, inputs)) * np.sqrt(2/inputs)
        rand_bias = np.random.random_sample((outputs,1)) * np.sqrt(2/inputs)
        self.weights = rand_weights
        self.bias = rand_bias

    def forward(self, input):
        return (self.weights @ input) + self.bias


a = linear(2, 3)
b = np.zeros((2,1))
print(a(b))
print(a.bias)